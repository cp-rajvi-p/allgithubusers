//
//  User.swift
//  NewPractical3.0
//
//  Created by Rajvi on 10/08/21.
//

import Foundation


struct User: Decodable {
    let id: Int
    let name: String
    let avatarUrl: String
    let url : String
    let followers: Int?
    let following: Int?
    let email: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "login"
        case avatarUrl = "avatar_url"
        case url = "url"
        case followers = "followers"
        case following = "following"
        case email = "email"
    }
}
